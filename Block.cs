﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace BlockchainAssignment
{
    class Block
    {
        public bool THREADING { get; set; } = true;                                 // Boolean to tell whether to use threading or not
        public List<Transaction> transactionList { get; set; }                  // List of transactions in this block
        public DateTime timeStamp { get; set; }                                 // Time of creation
        public int index { get; set; }                                          // Position of the block in the sequence of blocks
        public int difficulty { get; set; }                                    // An arbitrary number of 0's to proceed a hash value
        public long nonce { get; set; } = 0;                                    // Number used once for Proof-of-Work and mining
        public string hash { get; set; }                                        // The current blocks "identity"
        public string prevHash { get; set; }                                    // A reference pointer to the previous block
        public string merkleRoot { get; set; } = "0";                              // The merkle root of all transactions in the block
        public string minerAddress { get; set; }                                // Public Key (Wallet Address) of the Miner
        public double reward { get; set; } = 100;                                     // Simple fixed reward established by "Coinbase"
        // private int nonce = 0;
        public int nonce0 { get; set; } = 0;
        public int nonce1 { get; set; } = 1;
        public string finalHash { get; set; }
        public string finalHash0 { get; set; }
        public string finalHash1 { get; set; }
        private bool th1Fin = false, th2Fin = false;

        // Constructor which is passed the previous block
        public Block(Block lastBlock)
        {
            timeStamp = DateTime.Now;
            nonce = 0;
            index = lastBlock.index + 1;
            prevHash = lastBlock.hash;
            if (THREADING == true)
            {
                ThreadedMine();
                hash = finalHash;
            }
            else hash = Create256Hash();
            //    Create hash from index, prevhash and time
            transactionList = new List<Transaction>();
        }

        public Block(Block lastBlock, List<Transaction> TPool)
        {
            transactionList = new List<Transaction>();
            nonce = 0;
            timeStamp = DateTime.Now;
            index = lastBlock.index + 1;
            prevHash = lastBlock.hash;
            addFromPool(TPool, 2, "!");
            // Create hash from index, prevhash and time
        }

        // Constructor which is passed the index & hash of previous block
        public Block(int lastIndex, string lastHash)
        {
            nonce = 0;
            timeStamp = DateTime.Now;                      // new time
            index = lastIndex + 1;                         // increment on last block
            prevHash = lastHash;                               //needs fixing 
            hash = Create256Hash();                              //    Create hash from index, prevhash and time
        }

        // Constructor which is not passed anything
        public Block()
        {
            // This generates the Genesis Block 
            transactionList = new List<Transaction>();
            timeStamp = DateTime.Now;                                    // new time
            index = 0;                                                  //  First Block = 0 
            prevHash = string.Empty;                                   //   No Previous Hash  
            hash = Create256Mine();                              //    Create hash from index, prevhash and time
            difficulty = 4;
        }

        public Block(Block lastBlock, List<Transaction> TPool, string MinerAddress, int setting, string address)
        {
            transactionList = new List<Transaction>();
            nonce = 0;
            timeStamp = DateTime.Now;
            difficulty = lastBlock.difficulty;
            adjustdiff(lastBlock.timeStamp); // comment out this line to turn off adjustable difficulty
            index = lastBlock.index + 1;
            prevHash = lastBlock.hash;
            minerAddress = MinerAddress;
            TPool.Add(createRewardTransaction(TPool)); // Create and append the reward transaction
            addFromPool(TPool, setting, address);
            //    Create hash from index, prevhash and time
            merkleRoot = MerkleRoot(transactionList); // Calculate the merkle root of the blocks transactions

            if (THREADING == true)
            {
                ThreadedMine();
                hash = finalHash;
            }
            else hash = Create256Mine();//Create256Hash();
            Console.WriteLine("Here");
        }

        public override string ToString()
        {
            return ("\n\n\t\t[BLOCK START]"
                + "\nIndex: " + index
                + "\tTimestamp: " + timeStamp
                + "\nPrevious Hash: " + prevHash
                + "\n\t\t-- PoW --"
                + "\nDifficulty Level: " + difficulty
                + "\nNonce: " + nonce
                + "\nHash: " + hash + " " + finalHash
                + "\n\t\t-- Rewards --"
                + "\nReward: " + reward
                + "\nMiners Address: " + minerAddress
                + "\n\t\t-- " + transactionList.Count + " Transactions --"
                + "\nMerkle Root: " + merkleRoot
                + "\n" + string.Join("\n", transactionList)
                + "\n\t\t[BLOCK END]");
        }

        public string ReturnString()
        {
            return ("\n\n\t\t[BLOCK START]"
                + "\nIndex: " + index
                + "\tTimestamp: " + timeStamp
                + "\nPrevious Hash: " + prevHash
                + "\n\t\t-- PoW --"
                + "\nDifficulty Level: " + difficulty
                + "\nNonce: " + nonce
                + "\nHash: " + hash
                + "\n\t\t-- Rewards --"
                + "\nReward: " + reward
                + "\nMiners Address: " + minerAddress
                + "\n\t\t-- " + transactionList.Count + " Transactions --"
                + "\nMerkle Root: " + merkleRoot
                + "\n" + string.Join("\n", transactionList)
                + "\n\t\t[BLOCK END]");
        }

        public string readblock()
        {
            string s = "";
            s += ToString();
            foreach (Transaction T in transactionList)
            {
                s += "\n" + T.ToString();
            }
            return s;
        }

        public void add2TList(Transaction T)
        {
            transactionList.Add(T);
        }

        public void addFromPool(List<Transaction> TP, int mode, string address)
        {
            int LIMIT = 5;
            int idx = 0;
            while (transactionList.Count < LIMIT && TP.Count > 0)
            {
                if (mode == 0)
                {// greedy
                    //int idx = 0;
                    for (int i = 0; i < TP.Count; i++)
                    {
                        if (TP.ElementAt(i).Fee > TP.ElementAt(idx).Fee)
                        {
                            idx = i;
                        }
                    }
                    transactionList.Add(TP.ElementAt(idx));
                }
                else if (mode == 1)
                {// altruistic
                    for (int i = 0; (i < TP.Count) && (i < LIMIT); i++)
                    {
                        transactionList.Add(TP.ElementAt(i));
                    }
                }
                else if (mode == 2)
                {  //random      
                    Random random = new Random();
                    idx = random.Next(0, TP.Count);
                    transactionList.Add(TP.ElementAt(idx));
                }
                else if (mode == 3)
                {

                    //transactionList.Add(TP.ElementAt(0));
                    for (int i = 0; i < TP.Count && (transactionList.Count < LIMIT); i++)
                    {
                        if (TP.ElementAt(i).SenderAddress == address)
                        {
                            transactionList.Add(TP.ElementAt(i));
                        }
                        else if (TP.ElementAt(i).RecipientAddress == address)
                        {
                            transactionList.Add(TP.ElementAt(i));
                        }
                        else
                        {
                            // ONLY TAKE FROM THIS ADDRESS: 
                            // If take address as priority and then add up to get to 5 --> add mode = 2 here
                        }
                        //TP = TP.Except(this.transactionList).ToList();

                    }
                    Console.WriteLine("Endless loop");
                }
                else
                { // No Valid input, choose default --> Altruistic
                    mode = 1;
                }
                TP = TP.Except(transactionList).ToList();

            }

        }

        public string Create256Hash()
        { // i think this can be simplified // Simplified Heavily is "this" needed?
            SHA256 hasher;
            hasher = SHA256.Create();
            string input = index.ToString() + timeStamp.ToString() + prevHash + nonce + merkleRoot + reward.ToString();
            byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes((input)));

            string hash = string.Empty;

            foreach (byte x in hashByte)
            {
                hash += string.Format("{0:x2}", x);
            }
            return hash;
        }

        private string Create256Mine()
        {
            string hash = "";          // could also do Createhash here, then no need to nonce--
            string diffString = new string('0', difficulty);
            while (hash.StartsWith(diffString) == false)
            {
                hash = Create256Hash();
                nonce++;
            }
            nonce--;
            if (hash is null)
            {
                throw new IndexOutOfRangeException("No hash generated");
            }
            return hash;
        }

        public static string MerkleRoot(List<Transaction> transactionList)
        {
            // X => f(X) means given X return f(X)
            List<string> hashlist = transactionList.Select(t => t.Hash).ToList(); // Get a list of transaction hashlist for "combining"
            // Handle Blocks with...
            if (hashlist.Count == 0) // No transactions
            {
                return string.Empty;
            }
            else if (hashlist.Count == 1) // One transaction - hash with "self"
            {
                return HashCode.HashTools.combineHash(hashlist[0], hashlist[0]);
            }
            while (hashlist.Count != 1) // Multiple transactions - Repeat until tree has been traversed
            {
                List<string> merkleLeaves = new List<string>(); // Keep track of current "level" of the tree

                for (int i = 0; i < hashlist.Count; i += 2) // Step over neighbouring pair combining each
                {
                    if (i == hashlist.Count - 1)
                    {
                        merkleLeaves.Add(HashCode.HashTools.combineHash(hashlist[i], hashlist[i])); // Handle an odd number of leaves
                    }
                    else
                    {
                        merkleLeaves.Add(HashCode.HashTools.combineHash(hashlist[i], hashlist[i + 1])); // Hash neighbours leaves
                    }
                }
                hashlist = merkleLeaves; // Update the working "layer"
            }
            return hashlist[0]; // Return the root node
        }

        // Create reward for incentivising the mining of block
        public Transaction createRewardTransaction(List<Transaction> transactions)
        {
            double fees = transactions.Aggregate(0.0, (acc, t) => acc + t.Fee); // Sum all transaction fees
            return new Transaction("Mine Rewards", "", minerAddress, reward + fees, 0); // Issue reward as a transaction in the new block
        }

        /*FOR MULTI THREADING */
        public void ThreadedMine()
        {
            Thread th1 = new Thread(Mine0);
            Thread th2 = new Thread(Mine1);

            th1.Start();
            th2.Start();

            while (th1.IsAlive == true || th2.IsAlive == true) { Thread.Sleep(1); }

            //if (nonce0  < nonce1){
            if (finalHash1 is null)
            {
                nonce = nonce0;
                finalHash = finalHash0;
            }
            else
            {
                nonce = nonce1;
                finalHash = finalHash1;
            }
            if (finalHash is null)
            {
                Console.WriteLine(ReturnString());
                throw new Exception("NULL finalhash" +
                    " Nonce0: " + nonce0 +
                    " Nonce1: " + nonce1 +
                    " Nonce: " + nonce +
                    " finalhash0 " + finalHash0 +
                    " finalhash1: " + finalHash1 +
                    " NewHash: " + Create256Hash());

            }
            //Console.WriteLine("FINALHASH = " + finalHash);
            //return finalHash;
        }

        public void Mine0()
        {
            th1Fin = false;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            bool check = false;
            string newHash;
            string diffString = new string('0', difficulty);

            while (check == false)
            {
                newHash = Create256Hash(nonce0);
                if (newHash.StartsWith(diffString) == true)
                {
                    check = true;
                    finalHash0 = newHash;
                    Console.WriteLine("Block index: " + index);
                    Console.WriteLine("Thread 1 closed: Thread 1 found: " + finalHash0);
                    th1Fin = true;

                    Console.WriteLine(nonce0);
                    sw.Stop();
                    Console.WriteLine("Th1 mine:");
                    Console.WriteLine(sw.Elapsed);

                    return;
                }
                else if (th2Fin == true)
                {
                    Console.WriteLine("Thread 1 closed: Thread 2 found: " + finalHash1);
                    Thread.Sleep(1);
                    return;
                }
                else
                {
                    check = false;
                    nonce0 += 2;
                }
            }
            return;
        }

        public void Mine1()
        {
            th2Fin = false;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            bool check = false;
            string newHash;
            string diffString = new string('0', difficulty);
            while (check == false)
            {
                newHash = Create256Hash(nonce1);
                if (newHash.StartsWith(diffString) == true)
                {
                    check = true;
                    finalHash1 = newHash;
                    Console.WriteLine("Block index: " + index);
                    Console.WriteLine("Thread 2 closed: Thread 2 found: " + finalHash1);
                    th2Fin = true;

                    Console.WriteLine(nonce1);
                    sw.Stop();
                    Console.WriteLine("Th2 mine:");
                    Console.WriteLine(sw.Elapsed);

                    return;
                }
                else if (th1Fin == true)
                {
                    Console.WriteLine("Thread 2 closed: Thread 1 found: " + finalHash0);
                    Thread.Sleep(1);
                    return;
                }
                else
                {
                    check = false;
                    nonce1 += 2;
                }
            }
            return;
        }

        //Version of above method to take nonce as a parameter
        public string Create256Hash(int inNonce)
        {
            SHA256 hasher;
            hasher = SHA256.Create();
            string input = index.ToString() + timeStamp.ToString() + prevHash + inNonce + merkleRoot + reward.ToString();
            byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes((input)));
            string hash = string.Empty;

            foreach (byte x in hashByte)
            {
                hash += string.Format("{0:x2}", x);
            }
            return hash;
        }

        //Function to adjust the difficulty
        public void adjustdiff(DateTime lastTime)
        {
            //Gets the elapsed time between now and the last block mined
            DateTime startTime = DateTime.UtcNow;
            TimeSpan timeDiff = startTime - lastTime;

            //If the difference is less than 5 seconds, the difficulty is increased to attempt to increase the time
            if (timeDiff < TimeSpan.FromSeconds(5))
            {
                difficulty++;
                Console.WriteLine("Time since last mine");
                Console.WriteLine(timeDiff);
                Console.WriteLine("New Difficulty:");
                Console.WriteLine(difficulty);
            }
            //If the difference is more than 5 seconds, the difficulty is increased to attempt to decrease the time
            else if (timeDiff > TimeSpan.FromSeconds(5))
            {
                difficulty--;
                Console.WriteLine("Time since last mine");
                Console.WriteLine(timeDiff);
                Console.WriteLine("New Difficulty:");
                Console.WriteLine(difficulty);
            }

            //Difficulty can never be higher than 5 or lower than 0
            if (difficulty <= 0)
            {
                difficulty = 0;
                Console.WriteLine("Difficulty too low, new difficulty:");
                Console.WriteLine(difficulty);
            }
            else if (difficulty >= 6)
            {
                difficulty = 4;
                Console.WriteLine("Difficulty too high, new difficulty:");
                Console.WriteLine(difficulty);
            }
        }
    }
}