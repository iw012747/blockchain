﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace BlockchainAssignment
{
    class Transaction
    {
        public string Hash { get; set; } // The Hash
        public string Signature { get; set; } // Hash signed with private key of sender
        public string SenderAddress { get; set; } // Public key of sender
        public string RecipientAddress { get; set; } // public key of reciever 
        public DateTime TimeStamp { get; set; } // Time of transaction
        public double Amount { get; set; } // amount sent  // decimal Amount = 2.1M (suffix M needed )
        public double Fee { get; set; } // the fee added to transaction 

        public Transaction()
        {

        }
        public Transaction(string senderPublic, string senderPrivate, string recipientPublic, double amount, double fee)
        {
            TimeStamp = DateTime.Now;

            SenderAddress = senderPublic;

            RecipientAddress = recipientPublic;

            Amount = amount;

            Fee = fee;

            Hash = Create256Hash();

            Signature = Wallet.Wallet.CreateSignature(SenderAddress, senderPrivate, Hash);

        }

        private string Create256Hash()
        { // i think this can be simplified // Simplified Heavily is "this" needed?

            SHA256 hasher;
            hasher = SHA256.Create();

            /* Concatenate all transaction properties */
            string input = SenderAddress + RecipientAddress + TimeStamp.ToString() + Amount.ToString() + Fee.ToString();
            /* Apply the hash function to the "input" string */
            byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes((input)));

            string hash = string.Empty;


            /* Reformat to a string */
            foreach (byte x in hashByte)
            {
                hash += string.Format("{0:x2}", x);
            }
            return hash;

        }

        /* Two  similar implementations to print the contents of a transaction: one using the ToString Override*/
        public override string ToString()
        {
            return ("[TRANSACTION START]"
                + "\n\t  Timestamp: " + TimeStamp
                + "\n  -- Verification --"
                + "\n\t  Hash: " + Hash
                + "\n\t  Signature: " + Signature
                + "\n  -- Quantities --"
                + "\n\t  Transferred: " + Amount + " Energy Drink Induced Crypto Coin"
                + "\t  Fee: " + Fee
                + "\n  -- Participants --"
                + "\n\t  Sender: " + SenderAddress
                + "\n\t  Reciever: " + RecipientAddress
                + "\n  [TRANSACTION END]");
        }
        public string ReturnString()
        {
            return ("[TRANSACTION START]"
                + "\n\t  Timestamp: " + TimeStamp
                + "\n  -- Verification --"
                + "\n\t  Hash: " + Hash
                + "\n\t  Signature: " + Signature
                + "\n  -- Quantities --"
                + "\n\t  Transferred: " + Amount + " Energy Drink Induced Crypto Coin"
                + "\t  Fee: " + Fee
                + "\n  -- Participants --"
                + "\n\t  Sender: " + SenderAddress
                + "\n\t  Reciever: " + RecipientAddress
                + "\n  [TRANSACTION END]");

            /*("Requested by:  27016005 " +
                "\n Transaction Hash: " + Hash +
                "\n Digital Signature: " + Signature +
                "\n Timestamp: " + TimeStamp +
                "\n Transferred: " + Amount + 
                "\n Fees: "+Fee +
                "\n Sender Address: " + SenderAddress +
                "\n Receiver Address: "+ RecipientAddress +
                ". "
                );*/
        }

    }
}
